provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_dynamodb_table" "expends" {
  name         = "expends"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "partitionKey"
  range_key    = "sortKey"

  attribute {
    name = "partitionKey"
    type = "S"
  }

  attribute {
    name = "sortKey"
    type = "S"
  }
}
