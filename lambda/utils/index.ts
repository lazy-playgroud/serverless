import { APIGatewayProxyEvent } from 'aws-lambda'

export function parseBody<T = object>(event: APIGatewayProxyEvent): T {
  const { body } = event
  return body !== null && body !== undefined ? JSON.parse(body) : {}
}

export function pasePathParameters(event: APIGatewayProxyEvent) {
  const { pathParameters } = event
  return pathParameters !== null && pathParameters !== undefined ? pathParameters : {}
}

export function parseQueryStringParameters(event: APIGatewayProxyEvent) {
  const { queryStringParameters } = event
  return queryStringParameters !== null && queryStringParameters !== undefined ? queryStringParameters : {}
}

export function formatJson<T>(response: T, statusCode = 200) {
  return {
    statusCode,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(response)
  }
}

export function buildDateFormat(format: 'DATE' | 'TIME' | 'DATETIME', date?: string) {
  const dateParse = date !== undefined ? new Date(date) : new Date()
  const year = dateParse.getFullYear()
  const month = String(dateParse.getMonth() + 1).padStart(2, '0')
  const day = String(dateParse.getDate()).padStart(2, '0')
  const hour = String(dateParse.getHours()).padStart(2, '0')
  const minute = String(dateParse.getMinutes()).padStart(2, '0')
  const second = String(dateParse.getSeconds()).padStart(2, '0')
  const millisecond = String(dateParse.getMilliseconds()).padStart(3, '0')
  const dateFormatted = `${year}${month}${day}`
  const timeFormatted = `${hour}${minute}${second}${millisecond}`
  const datetimeFormatted = `${year}${month}${day}${hour}${minute}${second}${millisecond}`
  switch (format) {
    case 'DATE':
      return dateFormatted
    case 'TIME':
      return timeFormatted
    case 'DATETIME':
      return datetimeFormatted
    default:
      return dateFormatted
  }
}
