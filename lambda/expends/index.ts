import { APIGatewayEvent } from 'aws-lambda'
import { DynamoDB, PutItemCommand, UpdateItemCommand, UpdateItemCommandInput } from '@aws-sdk/client-dynamodb'
import {
  buildDateFormat,
  formatJson,
  parseBody,
  parseQueryStringParameters,
  pasePathParameters
} from '../utils'
import { Expend } from './types'
import 'reflect-metadata'
import { ExpendSchema } from './schemas'
import { validate } from 'class-validator'
import { randomUUID } from 'crypto'

const dynamodbClient = new DynamoDB({ region: 'ap-southeast-1' })
const TableName = 'expends'
const partitionKey = 'expends'

export async function list(event: APIGatewayEvent) {
  const queryString = parseQueryStringParameters(event)
  const { date } = queryString
  const dateParse = buildDateFormat('DATE', date)
  const paramQuery = {
    TableName,
    KeyConditionExpression: '#pk = :pk and begins_with(#sk, :sk)',
    ExpressionAttributeNames: {
      '#pk': 'pk',
      '#sk': 'sk'
    },
    ExpressionAttributeValues: {
      ':pk': { S: partitionKey },
      ':sk': { S: dateParse }
    }
  }
  const result = await dynamodbClient.query(paramQuery)
  const items = result.Items !== undefined ? result.Items : []
  return formatJson(items, 200)
}

export async function create(event: APIGatewayEvent) {
  const body = event.body ? JSON.parse(event.body) : {}
  const uuid = randomUUID()
  const dateParse = buildDateFormat('DATETIME')

  const Item: { [key: string]: { S: string } } = {
    partitionKey: { S: partitionKey },
    sortKey: { S: dateParse },
    date: { S: dateParse }
  }

  Object.entries(body).forEach(([key, value]) => {
    Item[key] = { S: value as string }
  })

  const params = {
    TableName,
    Item
  }

  await dynamodbClient.send(new PutItemCommand(params))
  console.timeEnd(`create an expend with id: ${uuid}`)

  const response = formatJson({ message: 'Expend created' }, 201)
  return response
}

export async function update(event: APIGatewayEvent) {
  const pathParameters = pasePathParameters(event)
  const { sortKey } = pathParameters

  const body = parseBody<Expend>(event)
  const validator = new ExpendSchema(body)
  const errors = await validate(validator)
  if (errors.length > 0) {
    return formatJson({ message: 'Validation error', errors }, 400)
  }

  const params: UpdateItemCommandInput = {
    TableName,
    Key: {
      partitionKey: { S: partitionKey },
      sortKey: { S: sortKey || '' }
    },
    UpdateExpression: 'SET #category = :category, #amount = :amount, #description = :description',
    ExpressionAttributeNames: {
      '#category': 'category',
      '#amount': 'amount',
      '#description': 'description'
    },
    ExpressionAttributeValues: {
      ':category': { S: validator.category },
      ':amount': { N: String(validator.amount) },
      ':description': { S: validator.description }
    },
    ReturnValues: 'ALL_NEW'
  }
  await dynamodbClient.updateItem(params)
  return formatJson({ message: 'Expend updated' }, 200)
}

export async function remove(event: APIGatewayEvent) {
  const pathParameters = pasePathParameters(event)
  const { sortKey } = pathParameters
  const params = {
    TableName,
    Key: {
      partitionKey: { S: partitionKey },
      sortKey: { S: sortKey || '' }
    }
  }
  await dynamodbClient.deleteItem(params)
  return formatJson({ message: 'Expend deleted' }, 200)
}
