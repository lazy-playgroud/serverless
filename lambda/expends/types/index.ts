export type Expend = {
  date: string
  category: string
  amount: number
  description: string
}
