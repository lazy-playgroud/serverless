import { IsDate, IsNumber, IsOptional, IsPositive, IsString } from 'class-validator'
import { Expend } from '../types'

export class ExpendSchema {
  @IsString()
  category: string

  @IsNumber()
  @IsPositive()
  amount: number

  @IsString()
  @IsOptional()
  description: string

  constructor(data: Expend) {
    this.category = data.category
    this.amount = data.amount
    this.description = data.description
  }
}
